import { MetricsPanelCtrl } from "app/plugins/sdk";
import _ from "lodash";
import rendering from "./rendering";

export class NetworkChartCtrl extends MetricsPanelCtrl {
  constructor(
    $scope,
    $injector,
    $rootScope,
    $interpolate,
    $sanitize,
    templateSrv,
    detangleSrvNew
  ) {
    super($scope, $injector);
    this.$rootScope = $rootScope;
    this.$interpolate = $interpolate;
    this.$sanitize = $sanitize;
    this.templateSrv = templateSrv;
    this.detangleSrv = detangleSrvNew;
    this.highlightedCircles = {};
    this.highlightedRects = {};
    this.highlightedTriangles = {};
    this.searchedFiles = [];
    this.searchedRefs = [];
    var panelDefaults = {
      color_scale: "schemeCategory10",
      first_color_selector: "index",
      first_color_regexp: "(.+?)\\/",

      second_color_selector: "index",
      second_color_regexp: "(.+?)\\/",

      combine_active: false,
      combine_method: "min",

      dynamic_radius: false,
      node_radius: 5,
      square_side_length: 10,

      dynamic_thickness: true,
      link_thickness: 1,

      link_distance: 20,

      hide_internal_relationships: false,

      remove_noise: false,
      noise: 0,

      show_defect_issues: true,

      nodes_remove_noise: false,
      nodes_noise: 0,

      first_filter_minumum_number_of_links: 0,
      second_filter_minumum_number_of_links: 0,
      colorOptions: [
        {
          key: "Bug",
          value: "#e41a1c",
        },
      ],
      colorOptionsData: [],
      additionalData: false,
      additionalDataQueryOrder: 0,
      additional_color_selector: "index",
      additional_color_regexp: "(.+?)\\/",
      additional_node_size: 50,
      issuedata_network: false,
      committed_issues: false,
      show_all_issues: false,
      locs_metric_name: "LOC",
    };

    _.defaults(this.panel, panelDefaults);

    //this.events.on('render', this.onRender.bind(this));
    this.events.on("data-received", this.onDataReceived.bind(this));
    this.events.on("data-error", this.onDataError.bind(this));
    this.events.on("data-snapshot-load", this.onDataReceived.bind(this));
    this.events.on("init-edit-mode", this.onInitEditMode.bind(this));

    this.onColorChange = this.onColorChange.bind(this);
  }

  onInitEditMode() {
    this.addEditorTab(
      "Options",
      "public/plugins/grafana-networkchart-panel/editor.html",
      2
    );
  }

  onDataError() {
    this.columnMap = [];
    this.columns = [];
    this.data = [];
    this.data2 = [];
    this.render();
  }

  onColorChange(styleIndex) {
    return (newColor) => {
      this.panel.colorOptions[styleIndex].value = newColor;
      this.render();
    };
  }

  colorSelectOptions() {
    var values = ["index", "regular expression"];

    if (!this.columns) return [];

    var selectors = _.map(this.columns, "text");

    selectors.splice(-1);

    return values.concat(selectors);
  }

  additionalColorSelectOptions() {
    var values = ["index", "regular expression"];

    if (!this.panel.additionalDataQueryOrder) return [];

    var selectors = _.map(this.additionalDataList.columns, "text");

    selectors.splice(-1);

    return values.concat(selectors);
  }

  additionalQuerySelectOptions() {
    return [...Array(this.numberOfDataList).keys()];
  }

  combineOptions() {
    if (!this.columns || this.columns.length < 2) return [];

    return [this.columns[0].text, this.columns[1].text];
  }

  /*
  onRender() {
    this.data = this.parsecolumnMap(this.columnMap);
  }
  */

  onDataReceived(dataList) {
    if (this.panel.issuedata_network) {
      if (!this.panel.show_all_issues) {
        let filterData = dataList[1];
        let acceptedIssues = [];
        for (let i = 0; i < filterData.rows.length; i++) {
          acceptedIssues.push(filterData.rows[i][0]);
        }
        if (this.panel.committed_issues) {
          dataList[0].rows = dataList[0].rows.filter((x) =>
            acceptedIssues.includes(x[1])
          );
        } else {
          dataList[0].rows = dataList[0].rows.filter(
            (x) =>
              acceptedIssues.includes(x[0]) || acceptedIssues.includes(x[1])
          );
        }
      }
      // dataList.splice(1, 1);
    }
    let data = dataList[0];

    if (!data) {
      this._error = "No data points.";
      return this.render();
    }

    if (data.type !== "table") {
      this._error = "Should be table fetch. Use terms only.";
      return this.render();
    }

    this._error = null;

    this.columnMap = data.columnMap;
    this.columns = data.columns;

    this.numberOfDataList = dataList.length;

    if (this.panel.additionalDataQueryOrder) {
      this.additionalDataList = dataList[this.panel.additionalDataQueryOrder];
    }

    if (!this.panel.first_term_tooltip && this.columns[0]) {
      this.panel.first_term_tooltip = "{{" + this.columns[0].text + "}}";
    }

    if (!this.panel.combine_to_show && this.columns[0]) {
      this.panel.combine_to_show = this.columns[0].text;
    }

    if (!this.panel.second_term_tooltip && this.columns[1]) {
      this.panel.second_term_tooltip = "{{" + this.columns[1].text + "}}";
    }
    let showPercentageOf = 100;
    let cohesionThreshold = 1;
    if (this.columns && this.columns.length >= 2) {
      let minIssues = this.templateSrv.replaceWithText(
        "$min_issues",
        this.panel.scopedVars
      );
      if (minIssues) {
        minIssues = minIssues.trim();
      }
      let shouldApplyMinIssues =
        minIssues !== "" && minIssues !== "-" && minIssues !== "$min_issues";
      if (shouldApplyMinIssues) {
        this.panel.second_filter_minumum_number_of_links = minIssues;
      } else {
        this.panel.second_filter_minumum_number_of_links = 0;
      }

      let combine = this.templateSrv.replaceWithText(
        "$combine",
        this.panel.scopedVars
      );
      if (combine) {
        combine = combine.trim();
      }
      let shouldApplyCombine =
        combine !== "" && combine !== "-" && combine !== "$combine";
      if (shouldApplyCombine) {
        this.panel.combine_active = combine === "true" ? true : false;
      } else {
        this.panel.combine_active = true;
      }

      let combinationType = this.templateSrv.replaceWithText(
        "$combination_type",
        this.panel.scopedVars
      );
      let shouldApplyCombinationType =
        combinationType !== "" &&
        combinationType !== "-" &&
        combinationType !== "$combination_type" &&
        combinationType !== "All";
      if (shouldApplyCombinationType) {
        this.panel.combine_active = true;
        switch (combinationType) {
          case "Issues Only":
            this.panel.combine_to_show = "@issueId";
            break;
          case "Contributors Only":
            this.panel.combine_to_show = "@author";
            break;
          case "Interval Only":
            let interval = this.templateSrv.replaceWithText(
              "$interval",
              this.panel.scopedVars
            );
            this.panel.combine_to_show = interval;
            break;
          default:
            this.panel.combine_to_show = "@path";
            break;
        }
      } else if (!shouldApplyCombine) {
        this.panel.combine_active = false;
      }

      let highlightIssues = this.templateSrv.replaceWithText(
        "$issue_title",
        this.panel.scopedVars
      );
      if (highlightIssues) {
        highlightIssues = highlightIssues.trim();
      }
      let shouldApplyIssueHighlight =
        highlightIssues !== "" &&
        highlightIssues !== "-" &&
        highlightIssues !== "$issue_title";
      if (shouldApplyIssueHighlight) {
        this.issues_highlight_text = highlightIssues;
      } else {
        this.issues_highlight_text = "";
      }

      let showDefects = this.templateSrv.replaceWithText(
        "$show_defect_issues",
        this.panel.scopedVars
      );
      if (showDefects) {
        showDefects = showDefects.trim();
      }
      let shouldShowDefects =
        showDefects !== "" &&
        showDefects !== "-" &&
        showDefects !== "$show_defect_issues";
      if (shouldShowDefects) {
        this.panel.show_defect_issues = showDefects === "yes";
      } else {
        this.panel.show_defect_issues = false;
      }

      let locMetric = this.templateSrv.replaceWithText(
        "$loc_metric",
        this.panel.scopedVars
      );
      if (locMetric) {
        locMetric = locMetric.trim();
      }
      let shouldSetLocMetric =
        locMetric !== "" &&
        locMetric !== "-" &&
        locMetric !== "$loc_metric";
      if (shouldSetLocMetric) {
        locMetric = locMetric.slice(1);
        if (locMetric === "nLOC") {
          this.panel.locs_metric_name = "LOC";
        } else{
          locMetric = locMetric.replace("codemetric_SQ_", "").replace("codemetric_DOC_", "");
          let humanReadableLocMetric = "";
          let metricNameArray = locMetric.split("_");
          for (let i = 0; i < metricNameArray.length; i++) {
            humanReadableLocMetric += metricNameArray[i].charAt(0).toUpperCase() + metricNameArray[i].slice(1).toLowerCase();
            if (i !== metricNameArray.length - 1) {
              humanReadableLocMetric += " ";
            }
          }
          this.panel.locs_metric_name = humanReadableLocMetric;
        }
      }

      let showAdditionalData = this.templateSrv.replaceWithText(
        "$show_additional_data",
        this.panel.scopedVars
      );
      if (showAdditionalData) {
        showAdditionalData = showAdditionalData.trim();
      }
      let shouldShowAdditionalData =
        showAdditionalData !== "" &&
        showAdditionalData !== "-" &&
        showAdditionalData !== "$show_additional_data";
      if (shouldShowAdditionalData) {
        this.panel.show_additional_data = showAdditionalData === "yes";
      } else {
        this.panel.show_additional_data = false;
      }

      let applyTimeSliderToDefectsData = this.templateSrv.replaceWithText(
        "$appy_timeslider_to_defects",
        this.panel.scopedVars
      );
      if (applyTimeSliderToDefectsData) {
        applyTimeSliderToDefectsData = applyTimeSliderToDefectsData.trim();
      }
      let applyTimeSliderToDefects =
        applyTimeSliderToDefectsData !== "" &&
        applyTimeSliderToDefectsData !== "-" &&
        applyTimeSliderToDefectsData !== "$show_defect_issues";
      if (applyTimeSliderToDefects) {
        this.panel.apply_time_slider_to_defects =
          applyTimeSliderToDefectsData === "yes";
      } else {
        this.panel.apply_time_slider_to_defects = false;
      }

      let couplingPercentage = this.templateSrv.replaceWithText(
        "$show_coupling_percentage",
        this.panel.scopedVars
      );
      if (couplingPercentage) {
        couplingPercentage = couplingPercentage.trim();
      }
      let applyPercentageFilter =
        couplingPercentage !== "" &&
        couplingPercentage !== "-" &&
        couplingPercentage !== "$show_coupling_percentage" &&
        this.isInt(couplingPercentage);
      if (applyPercentageFilter) {
        showPercentageOf = parseInt(couplingPercentage, 10);
      }

      let cohesionThresholdData = this.templateSrv.replaceWithText(
        "$cohesion_threshold",
        this.panel.scopedVars
      );
      if (cohesionThresholdData) {
        cohesionThresholdData = cohesionThresholdData.trim();
      }
      let applyCohesionThreshold =
        cohesionThresholdData !== "" &&
        cohesionThresholdData !== "-" &&
        cohesionThresholdData !== "$cohesion_threshold";
      if (applyCohesionThreshold) {
        cohesionThreshold = parseFloat(cohesionThresholdData);
      }

      let tempColorOptionsData = [];
      for (let i = 0; i < this.panel.colorOptions.length; i++) {
        let tempColorOption = this.panel.colorOptions[i];
        if (tempColorOption.key.startsWith("$")) {
          let realValueOfColorKey = this.templateSrv.replaceWithText(
            tempColorOption.key,
            this.panel.scopedVars
          );
          let realValueList = _.split(realValueOfColorKey, " + ");
          for (let j = 0; j < realValueList.length; j++) {
            tempColorOptionsData.push({
              key: realValueList[j],
              value: tempColorOption.value,
            });
          }
        } else {
          tempColorOptionsData.push(tempColorOption);
        }
      }

      this.panel.colorOptionsData = tempColorOptionsData;
    }
    let isIssueData = dataList.length > 4;
    let fixdataIndex = isIssueData ? 0 : 2;
    let saveCouplingDataSource = _.cloneDeep(dataList[3 - fixdataIndex]);
    this.panel.applyFolderLevel = true;
    let detangleresult = this.detangleSrv.adaptDataList(
      dataList,
      this.panel,
      "Table"
    );
    this.data = detangleresult[0].rows;
    if (dataList[1] && this.panel.show_defect_issues) {
      this.data = detangleresult[0].rows.concat(detangleresult[1].rows);
      if (detangleresult[2] && !this.panel.apply_time_slider_to_defects) {
        this.data = this.data.concat(detangleresult[2].rows);
      }
    }

    let view = this.templateSrv.replaceWithText("$view", this.panel.scopedVars);
    let shouldApplyViewSetting =
      view !== "" && view !== "All" && view !== "$view";
    if (shouldApplyViewSetting) {
      this.couplingData = [];
      let isOthersSelected = view.includes("Others");
      this.isOthersSelected = isOthersSelected;
      let couplingConfig = {
        coupling: true,
        applyFolderLevel: true,
      };
      const debtDataMap = this.detangleSrv.adaptDataList(
        [saveCouplingDataSource],
        couplingConfig,
        "RawData"
      );
      if (view.includes("Diamond") || isOthersSelected) {
        let tempCouplingData = [];
        debtDataMap.forEach((sourceAttr, key) => {
          tempCouplingData.push({
            id: key,
            couplingValue: sourceAttr.couplingValue,
          });
        });
        tempCouplingData = _.orderBy(
          tempCouplingData.filter(
            (x) => x.couplingValue !== 0 && x.couplingValue !== 1
          ),
          "couplingValue",
          "desc"
        );
        const sizeOfData = tempCouplingData.length;
        const startOfFilter = Math.floor(sizeOfData * (showPercentageOf / 100));
        tempCouplingData = tempCouplingData.slice(0, startOfFilter);
        this.couplingData = tempCouplingData.reduce(function (map, obj) {
          map[obj.id] = true;
          return map;
        }, {});
      }
      if (view.includes("Module") || isOthersSelected) {
        let tempCohesionData = [];
        debtDataMap.forEach((sourceAttr, key) => {
          tempCohesionData.push({
            id: key,
            cohesionValue: sourceAttr.cohesionValue,
          });
        });
        tempCohesionData = tempCohesionData.filter(
          (item) => item.cohesionValue <= cohesionThreshold
        );
        this.couplingData = Object.assign(
          this.couplingData,
          tempCohesionData.reduce(function (map, obj) {
            map[obj.id] = true;
            return map;
          }, {})
        );
      }
      if (dataList[4 - fixdataIndex]) {
        this.data2 = detangleresult[4 - fixdataIndex].rows;
      }
    } else {
      this.isOthersSelected = false;
      this.couplingData = null;
      if (dataList[1]) {
        this.data2 = detangleresult[1].rows;
      }
    }
    this.render(this.data, this.data2);
  }

  link(scope, elem, attrs, ctrl) {
    rendering(scope, elem, attrs, ctrl);
  }

  highlight() {
    this.render();
    this.prev_highlight_text = this.highlight_text;
    this.prev_issues_highlight_text = this.issues_highlight_text;
  }

  isInt(value) {
    // tslint:disable-next-line:no-bitwise
    return (
      !isNaN(value) &&
      (function (x) {
        return (x | 0) === x;
      })(parseFloat(value))
    );
  }

  addColorOption(group) {
    this.panel.colorOptions.push(group || {});
  }

  removeColorOption(group) {
    this.panel.colorOptions = _.without(this.panel.colorOptions, group);
    this.render();
  }
}

NetworkChartCtrl.templateUrl = "module.html";
