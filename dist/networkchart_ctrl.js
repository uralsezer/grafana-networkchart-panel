"use strict";

System.register(["app/plugins/sdk", "lodash", "./rendering"], function (_export, _context) {
  "use strict";

  var MetricsPanelCtrl, _, rendering, _createClass, NetworkChartCtrl;

  function _toConsumableArray(arr) {
    if (Array.isArray(arr)) {
      for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
        arr2[i] = arr[i];
      }

      return arr2;
    } else {
      return Array.from(arr);
    }
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }

  return {
    setters: [function (_appPluginsSdk) {
      MetricsPanelCtrl = _appPluginsSdk.MetricsPanelCtrl;
    }, function (_lodash) {
      _ = _lodash.default;
    }, function (_rendering) {
      rendering = _rendering.default;
    }],
    execute: function () {
      _createClass = function () {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }

        return function (Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);
          if (staticProps) defineProperties(Constructor, staticProps);
          return Constructor;
        };
      }();

      _export("NetworkChartCtrl", NetworkChartCtrl = function (_MetricsPanelCtrl) {
        _inherits(NetworkChartCtrl, _MetricsPanelCtrl);

        function NetworkChartCtrl($scope, $injector, $rootScope, $interpolate, $sanitize, templateSrv, detangleSrvNew) {
          _classCallCheck(this, NetworkChartCtrl);

          var _this = _possibleConstructorReturn(this, (NetworkChartCtrl.__proto__ || Object.getPrototypeOf(NetworkChartCtrl)).call(this, $scope, $injector));

          _this.$rootScope = $rootScope;
          _this.$interpolate = $interpolate;
          _this.$sanitize = $sanitize;
          _this.templateSrv = templateSrv;
          _this.detangleSrv = detangleSrvNew;
          _this.highlightedCircles = {};
          _this.highlightedRects = {};
          _this.highlightedTriangles = {};
          _this.searchedFiles = [];
          _this.searchedRefs = [];
          var panelDefaults = {
            color_scale: "schemeCategory10",
            first_color_selector: "index",
            first_color_regexp: "(.+?)\\/",

            second_color_selector: "index",
            second_color_regexp: "(.+?)\\/",

            combine_active: false,
            combine_method: "min",

            dynamic_radius: false,
            node_radius: 5,
            square_side_length: 10,

            dynamic_thickness: true,
            link_thickness: 1,

            link_distance: 20,

            hide_internal_relationships: false,

            remove_noise: false,
            noise: 0,

            show_defect_issues: true,

            nodes_remove_noise: false,
            nodes_noise: 0,

            first_filter_minumum_number_of_links: 0,
            second_filter_minumum_number_of_links: 0,
            colorOptions: [{
              key: "Bug",
              value: "#e41a1c"
            }],
            colorOptionsData: [],
            additionalData: false,
            additionalDataQueryOrder: 0,
            additional_color_selector: "index",
            additional_color_regexp: "(.+?)\\/",
            additional_node_size: 50,
            issuedata_network: false,
            committed_issues: false,
            show_all_issues: false,
            locs_metric_name: "LOC"
          };

          _.defaults(_this.panel, panelDefaults);

          //this.events.on('render', this.onRender.bind(this));
          _this.events.on("data-received", _this.onDataReceived.bind(_this));
          _this.events.on("data-error", _this.onDataError.bind(_this));
          _this.events.on("data-snapshot-load", _this.onDataReceived.bind(_this));
          _this.events.on("init-edit-mode", _this.onInitEditMode.bind(_this));

          _this.onColorChange = _this.onColorChange.bind(_this);
          return _this;
        }

        _createClass(NetworkChartCtrl, [{
          key: "onInitEditMode",
          value: function onInitEditMode() {
            this.addEditorTab("Options", "public/plugins/grafana-networkchart-panel/editor.html", 2);
          }
        }, {
          key: "onDataError",
          value: function onDataError() {
            this.columnMap = [];
            this.columns = [];
            this.data = [];
            this.data2 = [];
            this.render();
          }
        }, {
          key: "onColorChange",
          value: function onColorChange(styleIndex) {
            var _this2 = this;

            return function (newColor) {
              _this2.panel.colorOptions[styleIndex].value = newColor;
              _this2.render();
            };
          }
        }, {
          key: "colorSelectOptions",
          value: function colorSelectOptions() {
            var values = ["index", "regular expression"];

            if (!this.columns) return [];

            var selectors = _.map(this.columns, "text");

            selectors.splice(-1);

            return values.concat(selectors);
          }
        }, {
          key: "additionalColorSelectOptions",
          value: function additionalColorSelectOptions() {
            var values = ["index", "regular expression"];

            if (!this.panel.additionalDataQueryOrder) return [];

            var selectors = _.map(this.additionalDataList.columns, "text");

            selectors.splice(-1);

            return values.concat(selectors);
          }
        }, {
          key: "additionalQuerySelectOptions",
          value: function additionalQuerySelectOptions() {
            return [].concat(_toConsumableArray(Array(this.numberOfDataList).keys()));
          }
        }, {
          key: "combineOptions",
          value: function combineOptions() {
            if (!this.columns || this.columns.length < 2) return [];

            return [this.columns[0].text, this.columns[1].text];
          }
        }, {
          key: "onDataReceived",
          value: function onDataReceived(dataList) {
            if (this.panel.issuedata_network) {
              if (!this.panel.show_all_issues) {
                var filterData = dataList[1];
                var acceptedIssues = [];
                for (var i = 0; i < filterData.rows.length; i++) {
                  acceptedIssues.push(filterData.rows[i][0]);
                }
                if (this.panel.committed_issues) {
                  dataList[0].rows = dataList[0].rows.filter(function (x) {
                    return acceptedIssues.includes(x[1]);
                  });
                } else {
                  dataList[0].rows = dataList[0].rows.filter(function (x) {
                    return acceptedIssues.includes(x[0]) || acceptedIssues.includes(x[1]);
                  });
                }
              }
              // dataList.splice(1, 1);
            }
            var data = dataList[0];

            if (!data) {
              this._error = "No data points.";
              return this.render();
            }

            if (data.type !== "table") {
              this._error = "Should be table fetch. Use terms only.";
              return this.render();
            }

            this._error = null;

            this.columnMap = data.columnMap;
            this.columns = data.columns;

            this.numberOfDataList = dataList.length;

            if (this.panel.additionalDataQueryOrder) {
              this.additionalDataList = dataList[this.panel.additionalDataQueryOrder];
            }

            if (!this.panel.first_term_tooltip && this.columns[0]) {
              this.panel.first_term_tooltip = "{{" + this.columns[0].text + "}}";
            }

            if (!this.panel.combine_to_show && this.columns[0]) {
              this.panel.combine_to_show = this.columns[0].text;
            }

            if (!this.panel.second_term_tooltip && this.columns[1]) {
              this.panel.second_term_tooltip = "{{" + this.columns[1].text + "}}";
            }
            var showPercentageOf = 100;
            var cohesionThreshold = 1;
            if (this.columns && this.columns.length >= 2) {
              var minIssues = this.templateSrv.replaceWithText("$min_issues", this.panel.scopedVars);
              if (minIssues) {
                minIssues = minIssues.trim();
              }
              var shouldApplyMinIssues = minIssues !== "" && minIssues !== "-" && minIssues !== "$min_issues";
              if (shouldApplyMinIssues) {
                this.panel.second_filter_minumum_number_of_links = minIssues;
              } else {
                this.panel.second_filter_minumum_number_of_links = 0;
              }

              var combine = this.templateSrv.replaceWithText("$combine", this.panel.scopedVars);
              if (combine) {
                combine = combine.trim();
              }
              var shouldApplyCombine = combine !== "" && combine !== "-" && combine !== "$combine";
              if (shouldApplyCombine) {
                this.panel.combine_active = combine === "true" ? true : false;
              } else {
                this.panel.combine_active = true;
              }

              var combinationType = this.templateSrv.replaceWithText("$combination_type", this.panel.scopedVars);
              var shouldApplyCombinationType = combinationType !== "" && combinationType !== "-" && combinationType !== "$combination_type" && combinationType !== "All";
              if (shouldApplyCombinationType) {
                this.panel.combine_active = true;
                switch (combinationType) {
                  case "Issues Only":
                    this.panel.combine_to_show = "@issueId";
                    break;
                  case "Contributors Only":
                    this.panel.combine_to_show = "@author";
                    break;
                  case "Interval Only":
                    var interval = this.templateSrv.replaceWithText("$interval", this.panel.scopedVars);
                    this.panel.combine_to_show = interval;
                    break;
                  default:
                    this.panel.combine_to_show = "@path";
                    break;
                }
              } else if (!shouldApplyCombine) {
                this.panel.combine_active = false;
              }

              var highlightIssues = this.templateSrv.replaceWithText("$issue_title", this.panel.scopedVars);
              if (highlightIssues) {
                highlightIssues = highlightIssues.trim();
              }
              var shouldApplyIssueHighlight = highlightIssues !== "" && highlightIssues !== "-" && highlightIssues !== "$issue_title";
              if (shouldApplyIssueHighlight) {
                this.issues_highlight_text = highlightIssues;
              } else {
                this.issues_highlight_text = "";
              }

              var showDefects = this.templateSrv.replaceWithText("$show_defect_issues", this.panel.scopedVars);
              if (showDefects) {
                showDefects = showDefects.trim();
              }
              var shouldShowDefects = showDefects !== "" && showDefects !== "-" && showDefects !== "$show_defect_issues";
              if (shouldShowDefects) {
                this.panel.show_defect_issues = showDefects === "yes";
              } else {
                this.panel.show_defect_issues = false;
              }

              var locMetric = this.templateSrv.replaceWithText("$loc_metric", this.panel.scopedVars);
              if (locMetric) {
                locMetric = locMetric.trim();
              }
              var shouldSetLocMetric = locMetric !== "" && locMetric !== "-" && locMetric !== "$loc_metric";
              if (shouldSetLocMetric) {
                locMetric = locMetric.slice(1);
                if (locMetric === "nLOC") {
                  this.panel.locs_metric_name = "LOC";
                } else {
                  locMetric = locMetric.replace("codemetric_SQ_", "").replace("codemetric_DOC_", "");
                  var humanReadableLocMetric = "";
                  var metricNameArray = locMetric.split("_");
                  for (var _i = 0; _i < metricNameArray.length; _i++) {
                    humanReadableLocMetric += metricNameArray[_i].charAt(0).toUpperCase() + metricNameArray[_i].slice(1).toLowerCase();
                    if (_i !== metricNameArray.length - 1) {
                      humanReadableLocMetric += " ";
                    }
                  }
                  this.panel.locs_metric_name = humanReadableLocMetric;
                }
              }

              var showAdditionalData = this.templateSrv.replaceWithText("$show_additional_data", this.panel.scopedVars);
              if (showAdditionalData) {
                showAdditionalData = showAdditionalData.trim();
              }
              var shouldShowAdditionalData = showAdditionalData !== "" && showAdditionalData !== "-" && showAdditionalData !== "$show_additional_data";
              if (shouldShowAdditionalData) {
                this.panel.show_additional_data = showAdditionalData === "yes";
              } else {
                this.panel.show_additional_data = false;
              }

              var applyTimeSliderToDefectsData = this.templateSrv.replaceWithText("$appy_timeslider_to_defects", this.panel.scopedVars);
              if (applyTimeSliderToDefectsData) {
                applyTimeSliderToDefectsData = applyTimeSliderToDefectsData.trim();
              }
              var applyTimeSliderToDefects = applyTimeSliderToDefectsData !== "" && applyTimeSliderToDefectsData !== "-" && applyTimeSliderToDefectsData !== "$show_defect_issues";
              if (applyTimeSliderToDefects) {
                this.panel.apply_time_slider_to_defects = applyTimeSliderToDefectsData === "yes";
              } else {
                this.panel.apply_time_slider_to_defects = false;
              }

              var couplingPercentage = this.templateSrv.replaceWithText("$show_coupling_percentage", this.panel.scopedVars);
              if (couplingPercentage) {
                couplingPercentage = couplingPercentage.trim();
              }
              var applyPercentageFilter = couplingPercentage !== "" && couplingPercentage !== "-" && couplingPercentage !== "$show_coupling_percentage" && this.isInt(couplingPercentage);
              if (applyPercentageFilter) {
                showPercentageOf = parseInt(couplingPercentage, 10);
              }

              var cohesionThresholdData = this.templateSrv.replaceWithText("$cohesion_threshold", this.panel.scopedVars);
              if (cohesionThresholdData) {
                cohesionThresholdData = cohesionThresholdData.trim();
              }
              var applyCohesionThreshold = cohesionThresholdData !== "" && cohesionThresholdData !== "-" && cohesionThresholdData !== "$cohesion_threshold";
              if (applyCohesionThreshold) {
                cohesionThreshold = parseFloat(cohesionThresholdData);
              }

              var tempColorOptionsData = [];
              for (var _i2 = 0; _i2 < this.panel.colorOptions.length; _i2++) {
                var tempColorOption = this.panel.colorOptions[_i2];
                if (tempColorOption.key.startsWith("$")) {
                  var realValueOfColorKey = this.templateSrv.replaceWithText(tempColorOption.key, this.panel.scopedVars);
                  var realValueList = _.split(realValueOfColorKey, " + ");
                  for (var j = 0; j < realValueList.length; j++) {
                    tempColorOptionsData.push({
                      key: realValueList[j],
                      value: tempColorOption.value
                    });
                  }
                } else {
                  tempColorOptionsData.push(tempColorOption);
                }
              }

              this.panel.colorOptionsData = tempColorOptionsData;
            }
            var isIssueData = dataList.length > 4;
            var fixdataIndex = isIssueData ? 0 : 2;
            var saveCouplingDataSource = _.cloneDeep(dataList[3 - fixdataIndex]);
            this.panel.applyFolderLevel = true;
            var detangleresult = this.detangleSrv.adaptDataList(dataList, this.panel, "Table");
            this.data = detangleresult[0].rows;
            if (dataList[1] && this.panel.show_defect_issues) {
              this.data = detangleresult[0].rows.concat(detangleresult[1].rows);
              if (detangleresult[2] && !this.panel.apply_time_slider_to_defects) {
                this.data = this.data.concat(detangleresult[2].rows);
              }
            }

            var view = this.templateSrv.replaceWithText("$view", this.panel.scopedVars);
            var shouldApplyViewSetting = view !== "" && view !== "All" && view !== "$view";
            if (shouldApplyViewSetting) {
              this.couplingData = [];
              var isOthersSelected = view.includes("Others");
              this.isOthersSelected = isOthersSelected;
              var couplingConfig = {
                coupling: true,
                applyFolderLevel: true
              };
              var debtDataMap = this.detangleSrv.adaptDataList([saveCouplingDataSource], couplingConfig, "RawData");
              if (view.includes("Diamond") || isOthersSelected) {
                var tempCouplingData = [];
                debtDataMap.forEach(function (sourceAttr, key) {
                  tempCouplingData.push({
                    id: key,
                    couplingValue: sourceAttr.couplingValue
                  });
                });
                tempCouplingData = _.orderBy(tempCouplingData.filter(function (x) {
                  return x.couplingValue !== 0 && x.couplingValue !== 1;
                }), "couplingValue", "desc");
                var sizeOfData = tempCouplingData.length;
                var startOfFilter = Math.floor(sizeOfData * (showPercentageOf / 100));
                tempCouplingData = tempCouplingData.slice(0, startOfFilter);
                this.couplingData = tempCouplingData.reduce(function (map, obj) {
                  map[obj.id] = true;
                  return map;
                }, {});
              }
              if (view.includes("Module") || isOthersSelected) {
                var tempCohesionData = [];
                debtDataMap.forEach(function (sourceAttr, key) {
                  tempCohesionData.push({
                    id: key,
                    cohesionValue: sourceAttr.cohesionValue
                  });
                });
                tempCohesionData = tempCohesionData.filter(function (item) {
                  return item.cohesionValue <= cohesionThreshold;
                });
                this.couplingData = Object.assign(this.couplingData, tempCohesionData.reduce(function (map, obj) {
                  map[obj.id] = true;
                  return map;
                }, {}));
              }
              if (dataList[4 - fixdataIndex]) {
                this.data2 = detangleresult[4 - fixdataIndex].rows;
              }
            } else {
              this.isOthersSelected = false;
              this.couplingData = null;
              if (dataList[1]) {
                this.data2 = detangleresult[1].rows;
              }
            }
            this.render(this.data, this.data2);
          }
        }, {
          key: "link",
          value: function link(scope, elem, attrs, ctrl) {
            rendering(scope, elem, attrs, ctrl);
          }
        }, {
          key: "highlight",
          value: function highlight() {
            this.render();
            this.prev_highlight_text = this.highlight_text;
            this.prev_issues_highlight_text = this.issues_highlight_text;
          }
        }, {
          key: "isInt",
          value: function isInt(value) {
            // tslint:disable-next-line:no-bitwise
            return !isNaN(value) && function (x) {
              return (x | 0) === x;
            }(parseFloat(value));
          }
        }, {
          key: "addColorOption",
          value: function addColorOption(group) {
            this.panel.colorOptions.push(group || {});
          }
        }, {
          key: "removeColorOption",
          value: function removeColorOption(group) {
            this.panel.colorOptions = _.without(this.panel.colorOptions, group);
            this.render();
          }
        }]);

        return NetworkChartCtrl;
      }(MetricsPanelCtrl));

      _export("NetworkChartCtrl", NetworkChartCtrl);

      NetworkChartCtrl.templateUrl = "module.html";
    }
  };
});
//# sourceMappingURL=networkchart_ctrl.js.map
