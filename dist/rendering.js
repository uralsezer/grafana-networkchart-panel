"use strict";

System.register(["lodash", "app/core/app_events"], function (_export, _context) {
  "use strict";

  var _, appEvents;

  function link(scope, elem, attrs, ctrl) {
    function emit(field, value) {
      appEvents.emit("add-selection", {
        field: field,
        value: value
      });
    }

    var data, panel, svgWrapper, highlight_text, issues_highlight_text, issues_regex, files_regex, additionalData, maxWeight, couplingData, isOthersSelected, additionalDataList;

    var totals;
    var tooltipEle = elem.find(".nctooltip");
    var captionEle = elem.find(".caption");

    var theme = grafanaBootData.user.lightTheme ? "-light" : "-dark";

    elem = elem.find(".networkchart-panel");

    ctrl.events.on("render", function () {
      render();
    });

    function setElementHeight() {
      try {
        var height = ctrl.height || panel.height || ctrl.row.height;
        if (_.isString(height)) {
          height = parseInt(height.replace("px", ""), 10);
        }

        height -= 5; // padding
        height -= panel.title ? 24 : 9; // subtract panel title bar

        elem.css("height", height + "px");

        return true;
      } catch (e) {
        // IE throws errors sometimes
        return false;
      }
    }

    function showError(errorText) {
      var noData = elem.find(".no-data");
      if (errorText) {
        noData.text(errorText);
        noData.show();
      } else noData.hide();
    }

    function addZoom(svg) {
      svgWrapper = svg.select(".svg-wrapper");

      svg.call(d3.zoom().on("zoom", function () {
        var scale = d3.event.transform.k,
            translate = [d3.event.transform.x, d3.event.transform.y];

        svgWrapper.attr("transform", "translate(" + translate[0] + ", " + translate[1] + ") scale(" + scale + ")");
      }));
    }

    function y(d, i) {
      return 25 * (i + 1);
    }

    function y_plus_5(d, i) {
      return y(d, i) + 5;
    }

    function y_minus_5(d, i) {
      return y(d, i) - 5;
    }

    var tooltipEvals = [];

    function parseTooltip(tooltip, columnTexts) {
      var regExp = /{{(.*?)}}/g;
      var tooltipEval = ctrl.$sanitize(tooltip);
      var match;
      do {
        match = regExp.exec(tooltip);
        if (match) {
          var index = columnTexts.indexOf(match[1]);
          var replaceWith = index !== -1 ? "d[" + index + "]" : "";

          tooltipEval = tooltipEval.replace(match[1], replaceWith);
        }
      } while (match);

      return tooltipEval;
    }

    function createTooltipEvals(columnTexts) {
      var firstTooltip = panel.first_term_tooltip;

      var tooltipEvalText1 = firstTooltip && columnTexts && columnTexts.length ? parseTooltip(firstTooltip, columnTexts) : "{{d[0]}}";
      tooltipEvals[0] = ctrl.$interpolate(tooltipEvalText1);

      var secondTooltip = panel.second_term_tooltip;

      var tooltipEvalText2 = secondTooltip && columnTexts && columnTexts.length ? parseTooltip(secondTooltip, columnTexts) : "{{d[1]}}";
      tooltipEvals[1] = ctrl.$interpolate(tooltipEvalText2);
    }

    function combineFieldIndex(columnTexts) {
      if (panel.combine_to_show) {
        var showWhichIndex = columnTexts.indexOf(panel.combine_to_show);

        if (showWhichIndex !== -1) return showWhichIndex;
      }

      return 0;
    }

    function addNetworkChart() {
      if (typeof d3 == "undefined") return;

      var width = elem.width();
      var height = elem.height();

      var color = d3.scaleOrdinal(d3[panel.color_scale]);

      var colorForRectangles = d3.scaleOrdinal(d3["schemeSet3"]);

      var keyValueColorHash = panel.colorOptionsData.reduce(function (map, obj) {
        map[obj.key] = obj.value;
        return map;
      }, {});

      var nodeNameIndexHash = {};

      var noise = panel.remove_noise ? panel.noise : 0;
      var nodes_noise = panel.nodes_remove_noise ? panel.nodes_noise : 0;

      var noiseMin = 0;
      var noiseMax = 0;
      var isNoiseRange = false;

      var noiseArray = noise.toString().split("-");
      if (noiseArray.length === 2) {
        isNoiseRange = true;
        noiseMin = noiseArray[0];
        noiseMax = noiseArray[1];
      }

      var nodeNoiseMin = 0;
      var nodeNoiseMax = 0;
      var isNodeNoiseRange = false;

      if (nodes_noise) {
        var edgeNoiseArray = nodes_noise.toString().split("-");
        if (edgeNoiseArray.length === 2) {
          isNodeNoiseRange = true;
          nodeNoiseMin = edgeNoiseArray[0];
          nodeNoiseMax = edgeNoiseArray[1];
        }
      }

      var filter_first_link_numbers = panel.first_filter_minumum_number_of_links ? panel.first_filter_minumum_number_of_links : 0;
      var filter_second_link_numbers = panel.second_filter_minumum_number_of_links ? panel.second_filter_minumum_number_of_links : 0;

      //************************ Init Caption and Colors Data *************************/
      var colorSelections = {};
      var columns = [];
      var columnTexts = _.map(ctrl.columns, "text");

      var default_index1;
      var color_regexp1;
      var color_data_index1 = null;

      var default_index2;
      var color_regexp2;
      var color_data_index2 = null;

      var selector = panel.first_color_selector;
      if (selector === "index") {
        if (!panel.combine_active || combineFieldIndex(columnTexts) === 0) {
          default_index1 = columns.length;

          ctrl.columns[0].group = 0;
          columns.push(ctrl.columns[0]);
        }
      } else if (selector === "regular expression") color_regexp1 = new RegExp(panel.first_color_regexp);else color_data_index1 = columnTexts.indexOf(selector);

      selector = panel.second_color_selector;
      if (selector === "index") {
        if (!panel.combine_active || combineFieldIndex(columnTexts) === 1) {
          default_index2 = columns.length;

          ctrl.columns[1].group = 1;
          columns.push(ctrl.columns[1]);
        }
      } else if (selector === "regular expression") color_regexp2 = new RegExp(panel.second_color_regexp);else color_data_index2 = columnTexts.indexOf(selector);

      var additionalDataListRegexp;
      var additionalDataListIndex = null;
      var additionalDataListSecondRegexp;
      var additionalDataListSecondIndex = null;
      var additionalColumnTexts = [];
      if (panel.additionalData) {
        additionalColumnTexts = _.map(additionalDataList.columns, "text");
        selector = panel.additional_color_selector;
        if (selector === "regular expression") {
          additionalDataListRegexp = new RegExp(panel.additional_color_regexp);
        } else {
          additionalDataListIndex = additionalColumnTexts.indexOf(selector);
        }
        selector = panel.second_color_selector;
        if (selector === "regular expression") {
          additionalDataListSecondRegexp = new RegExp(panel.second_color_regexp);
        } else {
          additionalDataListSecondIndex = additionalColumnTexts.indexOf(selector);
        }
      }

      //************************ Tooltips *************************/

      createTooltipEvals(columnTexts);

      var tooltip = d3.select(tooltipEle[0]).style("opacity", 0);

      function showTooltip(d) {
        tooltip.transition().duration(200).style("opacity", 0.8);

        tooltip.html(d.tooltip).style("width", d.tooltip.length * 7 + "px").style("left", d3.event.pageX - 400 + "px").style("top", d3.event.pageY - 50 + "px");
      }

      function hideTooltip(d) {
        tooltip.transition().duration(500).style("opacity", 0);
      }

      //************************ Main Graph *************************/

      var svg = d3.select(elem[0]);
      // var context = svg.node().getContext("2d");

      d3.select("#clearButton").on("click", function () {
        ctrl.highlightedRects = {};
        ctrl.highlightedCircles = {};
        ctrl.highlightedTriangles = {};
        d3.select("#clearButton").style("display", "none");
        decidePrintButtonVisibility();
        highlightSelectedPatterns();
      });
      function getHighlightedItems() {
        var highlightedRows = [];
        var printKeys = ctrl.searchedFiles.concat(ctrl.searchedRefs).concat(Object.keys(ctrl.highlightedCircles)).concat(Object.keys(ctrl.highlightedRects)).concat(Object.keys(ctrl.highlightedTriangles));
        printKeys.forEach(function (printKey) {
          var nodeItem = totals[printKey];
          nodeItem.links.forEach(function (linkedItem) {
            var tempLink = linkData.find(function (x) {
              return x.target.id === linkedItem && x.source.id === printKey || x.target.id === printKey && x.source.id === linkedItem;
            });
            if (tempLink) {
              highlightedRows.push([printKey, linkedItem, tempLink.value]);
            }
          });
        });
        return highlightedRows;
      }

      d3.select("#showHighlightedFiles").on("click", function () {
        var exportRows = getHighlightedItems();
        var relations = Array.from(new Set(exportRows.map(function (x) {
          return allTooptips[x[0]] + "\n" + allTooptips[x[1]] + "\n" + x[2];
        }))).join("\n\n");
        var modelObj = {
          title: "Highlighted Items",
          description: relations
        };
        appEvents.emit("show-modal", {
          src: "public/app/features/plugins/partials/long_text.html",
          model: modelObj
        });
      });

      function downloadCSV(csv, filename) {
        var csvFile;
        var downloadLink;
        csvFile = new Blob([csv], { type: "text/csv" });
        downloadLink = document.createElement("a");
        downloadLink.download = filename;
        downloadLink.href = window.URL.createObjectURL(csvFile);
        downloadLink.style.display = "none";
        document.body.appendChild(downloadLink);
        downloadLink.click();
      }

      addZoom(svg);

      //************************ Links between nodes *************************/

      totals = {};
      var linkData = [];
      var nodesData = [];
      var nodesData2 = []; //Second group nodes
      var additionalNodesData = [];

      var doWeFilterOnEdgeCounts = filter_first_link_numbers || filter_second_link_numbers;

      var allTooptips = {};

      if (panel.combine_active) {
        var sourceIndex = combineFieldIndex(columnTexts);
        var targetIndex = +!sourceIndex; // 0 -> 1     1,2,... => 0

        var refCombination = false;

        if (["@issueId", "@author", "@commits", "@days", "@3days", "@5days", "@weeks"].includes(panel.combine_to_show)) {
          refCombination = true;
        }
        var allSources = {};
        var allTargets = {};
        var totalsFilterHash = {};

        _.forEach(data, function (d) {
          var value = d[d.length - 1];
          //No value
          if (!isNoiseRange) {
            if (!value || value < noise) {
              return;
            }
          }
          if (isNoiseRange) {
            if (!value || value < noiseMin || value > noiseMax) {
              return;
            }
          }
          var source = d[sourceIndex];
          var target = d[targetIndex];

          if (couplingData) {
            var tempCheckNode = source;
            if (refCombination) {
              tempCheckNode = target;
            }
            if (!isOthersSelected && !couplingData[tempCheckNode] || isOthersSelected && couplingData[tempCheckNode]) {
              return;
            }
          }

          initHash(allSources, source);
          initHash(allTargets, target);

          allSources[source][target] = value;
          allTargets[target][source] = value;

          allSources[source].tooltip = tooltipEvals[sourceIndex]({ d: d });
          allTooptips[source] = tooltipEvals[sourceIndex]({ d: d });

          if (color_data_index1 !== null || color_regexp1) allSources[source].group = getGroup(d, sourceIndex);

          if (doWeFilterOnEdgeCounts) {
            setTotalsHash(totalsFilterHash, source, value, target);
            setTotalsHash(totalsFilterHash, target, value, source);
          }
        });

        var combineMethod = _[panel.combine_method];

        var relations = {};

        for (var source in allSources) {
          initHash(relations, source);
          var currentRel = relations[source];

          for (var target in allSources[source]) {
            if (target === "group" || target === "tooltip") continue;

            if (doWeFilterOnEdgeCounts && (totalsFilterHash[source].count < filter_first_link_numbers || totalsFilterHash[target].count < filter_second_link_numbers)) {
              continue;
            }

            var value = allSources[source][target];

            for (var sourceFromTarget in allTargets[target]) {
              //Already calculated at the other end
              if (relations[sourceFromTarget]) continue;

              if (panel.hide_internal_relationships && allSources[source].group === allSources[sourceFromTarget].group) continue;

              if (!currentRel[sourceFromTarget]) currentRel[sourceFromTarget] = 0;

              var param = [value, allTargets[target][sourceFromTarget]];
              currentRel[sourceFromTarget] += combineMethod(param);
            }
          }
        }

        for (var relation1 in relations) {
          for (var relation2 in relations[relation1]) {
            var value = relations[relation1][relation2];

            setTotalsHash(totals, relation1, value, relation2);
            setTotalsHash(totals, relation2, value, relation1);
          }
        }

        for (var relation1 in relations) {
          for (var relation2 in relations[relation1]) {
            var addFirst = totals[relation1].value > nodes_noise;
            if (isNodeNoiseRange) addFirst = totals[relation1].value > nodeNoiseMin && totals[relation1].value < nodeNoiseMax;

            var addSecond = totals[relation2].value > nodes_noise;
            if (isNodeNoiseRange) addSecond = totals[relation2].value > nodeNoiseMin && totals[relation2].value < nodeNoiseMax;

            if (!addFirst && !addSecond) continue;

            if (refCombination) {
              nodesData.push({
                id: relation1,
                group: allSources[relation1].group,
                tooltip: allSources[relation1].tooltip + getTotalTooltip(relation1)
              });

              nodesData.push({
                id: relation2,
                group: allSources[relation2].group,
                tooltip: allSources[relation2].tooltip + getTotalTooltip(relation2)
              });
            } else {
              nodesData2.push({
                id: relation1,
                group: allSources[relation1].group,
                tooltip: allSources[relation1].tooltip + getTotalTooltip(relation1)
              });

              nodesData2.push({
                id: relation2,
                group: allSources[relation2].group,
                tooltip: allSources[relation2].tooltip + getTotalTooltip(relation2)
              });
            }

            var value = relations[relation1][relation2];

            linkData.push({
              id: relation1 + relation2,
              source: relation1,
              target: relation2,
              value: value,
              tooltip: relation1 + " <=> " + relation2 + "<br>" + value
            });
          }
        }
      } else {
        var weightHash = {};
        maxWeight = 0;
        _.forEach(additionalData, function (d) {
          var tempData = d[0];
          var value = d[d.length - 1];
          if (value > maxWeight) {
            maxWeight = value;
          }
          weightHash[tempData] = value;
        });
        _.forEach(data, function (d) {
          var value = d[d.length - 1];

          if (!isNoiseRange) {
            if (!value || value < noise) {
              return;
            }
          }
          if (isNoiseRange) {
            if (!value || value < noiseMin || value > noiseMax) {
              return;
            }
          }
          setTotalsHash(totals, d[0], value, d[1]);
          setTotalsHash(totals, d[1], value, d[0]);
        });
        _.forEach(data, function (d) {
          var value = d[d.length - 1];

          if (!isNoiseRange) {
            if (!value || value < noise) {
              return;
            }
          }
          if (isNoiseRange) {
            if (!value || value < noiseMin || value > noiseMax) {
              return;
            }
          }

          var firstNode = d[0];
          var secondNode = d[1];
          if (couplingData) {
            if (!isOthersSelected && !couplingData[secondNode] || isOthersSelected && couplingData[secondNode]) {
              return;
            }
          }

          if (nodes_noise) {
            var addFirst = totals[firstNode].value > nodes_noise;
            if (isNodeNoiseRange) addFirst = totals[firstNode].value > nodeNoiseMin && totals[firstNode].value < nodeNoiseMax;

            var addSecond = totals[secondNode].value > nodes_noise;
            if (isNodeNoiseRange) addSecond = totals[secondNode].value > nodeNoiseMin && totals[secondNode].value < nodeNoiseMax;

            if (!addFirst && !addSecond) return;
          }

          //Should I decrement the second link number count when first is omited or vice versa
          if (totals[firstNode].count < filter_first_link_numbers || totals[secondNode].count < filter_second_link_numbers) {
            return;
          }

          linkData.push({
            id: firstNode + secondNode,
            source: firstNode,
            target: secondNode,
            value: value,
            tooltip: firstNode + " <=> " + secondNode + " <br> " + value
          });

          nodesData.push({
            id: firstNode,
            group: getGroup(d, 0),
            tooltip: tooltipEvals[0]({ d: d }) + getTotalTooltip(firstNode)
          }); //columns[i].text
          allTooptips[firstNode] = tooltipEvals[0]({ d: d });
          var tempWeight = weightHash[secondNode];
          nodesData2.push({
            id: secondNode,
            group: getGroup(d, 1),
            weight: tempWeight,
            tooltip: tooltipEvals[1]({ d: d }) + getTotalTooltip(secondNode, tempWeight)
          });
          allTooptips[secondNode] = tooltipEvals[1]({ d: d });
        });
      }

      if (panel.additionalData && panel.show_additional_data) {
        var additionalTooltipText = panel.additional_tooltip && additionalColumnTexts && additionalColumnTexts.length ? parseTooltip(panel.additional_tooltip, additionalColumnTexts) : "{{d[0]}}";
        var additionalTooltipEval = ctrl.$interpolate(additionalTooltipText);
        var secondaryNodeHash = _.map(nodesData2, "id");
        var primaryNodeHash = _.map(nodesData, "id");

        _.forEach(additionalDataList.rows, function (d) {
          var value = d[d.length - 1];
          var firstNode = d[0];
          var secondNode = d[1];
          setTotalsHash(totals, firstNode, value, secondNode);
          setTotalsHash(totals, secondNode, value, firstNode);

          if (!secondaryNodeHash.includes(secondNode) && !primaryNodeHash.includes(secondNode)) {
            return;
          }
          linkData.push({
            id: firstNode + secondNode,
            source: firstNode,
            target: secondNode,
            value: value,
            tooltip: firstNode + " <=> " + secondNode + " <br> " + value
          });

          additionalNodesData.push({
            id: firstNode,
            group: getAdditionalGroup(d, 0),
            tooltip: additionalTooltipEval({ d: d }) + getTotalTooltip(firstNode)
          });
          allTooptips[firstNode] = additionalTooltipEval({ d: d });
        });
      }

      nodesData = _.uniqBy(nodesData, function (d) {
        return d.id;
      });
      nodesData2 = _.uniqBy(nodesData2, function (d) {
        return d.id;
      });
      additionalNodesData = _.uniqBy(additionalNodesData, function (d) {
        return d.id;
      });
      d3.select("#printHighlightedFiles").on("click", function () {
        var exportRows = getHighlightedItems();
        downloadCSV(Array.from(new Set(exportRows.map(function (x) {
          return allTooptips[x[0]] + "," + allTooptips[x[1]] + "," + x[2];
        }))).join("\n"), "HighlightedFiles.csv");
      });
      //************************ Links d3 *************************/

      var linkUpdate = svg.select(".links").selectAll("line").data(linkData, function (d) {
        return d.id;
      });

      // EXIT
      // Remove old elements as needed.
      linkUpdate.exit().remove();

      // ENTER
      // Create new elements as needed.
      var enter = linkUpdate.enter().append("line").attr("class", "line" + theme).attr("stroke", grafanaBootData.user.lightTheme ? "black" : "white").attr("opacity", 0.2).on("mouseover", showTooltip).on("mouseout", hideTooltip);

      /*
      enter
        .append("title")
        .text(d => d.id)
      */

      // ENTER + UPDATE

      var maxValue = _.reduce(linkData, function (max, d) {
        var log = d.value;
        if (log > max) return log;

        return max;
      }, 0);

      var maxValueLogged = Math.log(maxValue);

      function getLinkThickness(d) {
        if (panel.dynamic_thickness) {
          return 1 + d.value * 10 / maxValue;
        }
        return panel.link_thickness;
      }

      linkUpdate = linkUpdate.merge(enter).attr("stroke-width", function (d) {
        return getLinkThickness(d);
      });

      //************************ NODES d3 *************************/

      //************************ Circles d3 *************************/

      var radius = panel.node_radius;

      var circleUpdate = svg.select(".nodes").selectAll("circle").data(nodesData, function (d) {
        return d.id + "-node";
      });

      // EXIT
      // Remove old elements as needed.
      circleUpdate.exit().remove();

      // ENTER
      // Create new elements as needed.
      var circleEnter = circleUpdate.enter().append("circle").on("mouseover", showTooltip).on("mouseout", hideTooltip).on("click", clicked).call(d3.drag().on("start", dragstarted).on("drag", dragged).on("end", dragended));

      /*
      circleEnter
          .append("title")
          .text(d => d.id);
      */

      // ENTER + UPDATE
      circleUpdate = circleUpdate.merge(circleEnter)
      //.selectAll("circle")
      .attr("r", radius) // TODO use cummulative value for this
      .attr("fill", function (d) {
        return d.group !== undefined ? nodeNameIndexHash[d.group] ? keyValueColorHash[nodeNameIndexHash[d.group]] : color(d.group) : color(0);
      });

      //************************ Triangles d3 *************************/

      var triangleUpdates = svg.select(".nodes").selectAll("path").data(additionalNodesData, function (d) {
        return d.id + "-node";
      });

      // EXIT
      // Remove old elements as needed.
      triangleUpdates.exit().remove();

      // ENTER
      // Create new elements as needed.
      var triangleEnter = triangleUpdates.enter().append("path").on("mouseover", showTooltip).on("mouseout", hideTooltip).on("click", clicked).call(d3.drag().on("start", dragstarted).on("drag", dragged).on("end", dragended));

      var triangle = d3.symbol().type(d3.symbolTriangle).size(25);

      triangleUpdates = triangleUpdates.merge(triangleEnter).attr("d", function (d) {
        triangle.size(panel.additional_node_size);
        return triangle();
      }).attr("fill", function (d) {
        return d.group !== undefined ? nodeNameIndexHash[d.group] ? keyValueColorHash[nodeNameIndexHash[d.group]] : color(d.group) : color(0);
      });

      //************************ Rectangles d3 *************************/

      var squareSide = panel.square_side_length;

      var rectUpdate = svg.select(".nodes").selectAll("rect").data(nodesData2, function (d) {
        return d.id + "-node";
      });

      // EXIT
      // Remove old elements as needed.
      rectUpdate.exit().remove();

      // ENTER
      // Create new elements as needed.
      var rectEnter = rectUpdate.enter().append("rect").on("mouseover", showTooltip).on("mouseout", hideTooltip).on("click", clicked).call(d3.drag().on("start", dragstarted).on("drag", dragged).on("end", dragended));

      // ENTER + UPDATE
      rectUpdate = rectUpdate.merge(rectEnter).attr("x", function (d) {
        return d.weight ? -1 * (squareSide + d.weight * squareSide / maxWeight) / 2 : -squareSide / 2;
      }).attr("y", function (d) {
        return d.weight ? -1 * (squareSide + d.weight * squareSide / maxWeight) / 2 : -squareSide / 2;
      }).attr("width", function (d) {
        return d.weight ? squareSide + d.weight * squareSide / maxWeight : squareSide;
      }).attr("height", function (d) {
        return d.weight ? squareSide + d.weight * squareSide / maxWeight : squareSide;
      }).attr("fill", function (d) {
        return d.group ? colorForRectangles(d.group) : colorForRectangles(0);
      });

      var distance = panel.link_distance || 20;

      var simulation = d3.forceSimulation().force("link", d3.forceLink().id(function (d) {
        return d.id;
      })
      // .distance(distance)
      // .strength(d => {
      //
      //   if(!d.value)
      //     return 0.01;
      //
      //   var strength = Math.log(d.value)/maxValueLogged;
      //   if(strength < 0.01)
      //     return 0.01;
      //
      //   return strength;
      // })
      ).force("charge", d3.forceManyBody()).force("center", d3.forceCenter(width / 2, height / 2));

      simulation.nodes(nodesData.concat(nodesData2).concat(additionalNodesData)).on("tick", ticked);

      simulation.force("link").links(linkData);

      //************************ Add Caption Colors *************************/
      var captions = d3.select(captionEle[0]);

      /*
      var columnsSorted = columns;
      var secondColumsSorted;
       if(!panel.combine_active)
      {
        columnsSorted = _.filter(columns, ['group', 0 ]);
        secondColumsSorted = _.filter(columns, 'group');
      }
       columnsSorted = _.sortBy(columnsSorted,"text")
       if(secondColumsSorted)
        secondColumsSorted = _.sortBy(secondColumsSorted,"text")
       */
      if (panel.issuedata_network) {
        columns = columns.filter(function (x) {
          return x.group >= 0;
        });
      }

      captions.attr("height", y(null, columns.length));

      var columnsSorted = _.sortBy(columns, ["group", "text"]);

      var captionsUpdate = captions.selectAll("g").data(columnsSorted, function (d, i) {
        return d.text + i;
      });

      // EXIT
      // Remove old elements as needed.
      captionsUpdate.exit().remove();

      // ENTER
      // Create new elements as needed.
      var captionsEnter = captionsUpdate.enter().append("g");

      captionsEnter.append("text").attr("class", "captions-text" + theme).attr("x", 25).attr("y", y_plus_5);

      var captionsMerged = captionsUpdate.merge(captionsEnter);

      captionsMerged.selectAll("text").text(function (d) {
        return d.text;
      });
      captionsMerged.append(function (d) {
        var ele = d.group === -1 ? "path" : d.group === 0 ? "circle" : "rect";
        return d3.creator(ele).apply(this);
      });

      var colorTexts = _.map(columns, "text");
      var columnsSortedTexts = _.map(columnsSorted, "text");

      // ENTER + UPDATE
      captionsMerged.selectAll("circle").attr("r", 7).attr("cx", 15).attr("cy", function (d) {
        return 25 * (columnsSortedTexts.indexOf(d.text) + 1);
      }).attr("fill", function (d) {
        var indexOfSelected = colorTexts.indexOf(d.text);
        return nodeNameIndexHash[indexOfSelected] ? keyValueColorHash[nodeNameIndexHash[indexOfSelected]] : color(colorTexts.indexOf(d.text));
      });

      if (!panel.issuedata_network) {
        captionsMerged.selectAll("path").attr("d", function (d) {
          triangle.size(panel.additional_node_size);
          return triangle();
        }).attr("transform", function (d) {
          return "translate(" + 14 + "," + 25 * (columnsSortedTexts.indexOf(d.text) + 1) + ")";
        }).attr("fill", function (d) {
          var indexOfSelected = colorTexts.indexOf(d.text);
          return nodeNameIndexHash[indexOfSelected] ? keyValueColorHash[nodeNameIndexHash[indexOfSelected]] : color(colorTexts.indexOf(d.text));
        });
      }

      captionsMerged.selectAll("rect").attr("x", 10).attr("y", function (d) {
        return 25 * (columnsSortedTexts.indexOf(d.text) + 1) - 5;
      }).attr("width", 10).attr("height", 10).attr("fill", function (d) {
        return colorForRectangles(colorTexts.indexOf(d.text));
      });

      function checkHighlight(d) {
        return files_regex.test(d.tooltip.toLowerCase());
      }

      function checkHighlightIssues(d) {
        return issues_regex.test(d.tooltip.toLowerCase());
      }

      if (ctrl.issues_highlight_text) {
        issues_highlight_text = ctrl.issues_highlight_text.toLowerCase();
        issues_regex = new RegExp(issues_highlight_text);

        simulation.alphaTarget(0);
        ctrl.searchedRefs = [];
        Object.keys(totals).forEach(function (nodeItemKey) {
          if (!issues_regex.test(nodeItemKey.toLowerCase())) {
            return;
          }
          ctrl.searchedRefs.push(nodeItemKey);
        });
        circleUpdate.transition().duration(1000).delay(2000).attr("r", function (d) {
          return checkHighlightIssues(d) ? radius * 7 : radius;
        }).attr("stroke", function (d) {
          return checkHighlightIssues(d) ? "black" : "";
        }).attr("stroke-width", function (d) {
          return checkHighlightIssues(d) ? 2 : "";
        }).transition().duration(1000).delay(1000).attr("r", radius);
        decidePrintButtonVisibility();
      } else if (ctrl.prev_issues_highlight_text) {
        circleUpdate.attr("stroke", "").attr("stroke-width", "");
        rectUpdate.attr("stroke", "").attr("stroke-width", "");
        ctrl.searchedRefs = [];
        decidePrintButtonVisibility();
      }
      var firstLevelHighlights = [];
      if (ctrl.highlight_text) {
        highlight_text = ctrl.highlight_text.toLowerCase();
        files_regex = new RegExp(highlight_text);
        //stop simiulation
        simulation.alphaTarget(0);
        firstLevelHighlights = [];
        ctrl.searchedFiles = [];
        if (panel.combine_active) {
          Object.keys(totals).forEach(function (nodeItemKey) {
            if (!files_regex.test(nodeItemKey.toLowerCase())) {
              return;
            }
            var nodeItem = totals[nodeItemKey];
            ctrl.searchedFiles.push(nodeItemKey);
            firstLevelHighlights.push(nodeItemKey);
            firstLevelHighlights = firstLevelHighlights.concat(nodeItem.links);
          });
        } else {
          var relatedNodes = [];
          Object.keys(totals).forEach(function (nodeItemKey) {
            if (!files_regex.test(nodeItemKey.toLowerCase())) {
              return;
            }
            var nodeItem = totals[nodeItemKey];
            ctrl.searchedFiles.push(nodeItemKey);
            nodeItem.links.forEach(function (linkedItem) {
              if (linkData.some(function (x) {
                return x.target.id === linkedItem && x.source.id === nodeItemKey || x.target.id === nodeItemKey && x.source.id === linkedItem;
              })) {
                relatedNodes.push(linkedItem);
              }
            });
          });
          relatedNodes = _.uniq(relatedNodes);
          relatedNodes.forEach(function (nodeItemKey) {
            var nodeItem = totals[nodeItemKey];
            firstLevelHighlights.push(nodeItemKey);
            firstLevelHighlights = firstLevelHighlights.concat(nodeItem.links);
          });
        }
        firstLevelHighlights = _.uniq(firstLevelHighlights);
        circleUpdate.transition().duration(1).delay(1).attr("opacity", function (d) {
          return firstLevelHighlights.includes(d.id) ? 1.0 : 0.4;
        });

        rectUpdate.transition().duration(1000).delay(2000).attr("width", function (d) {
          return checkHighlight(d) ? squareSide * 7 : squareSide;
        }).attr("height", function (d) {
          return checkHighlight(d) ? squareSide * 7 : squareSide;
        }).attr("stroke", function (d) {
          return checkHighlight(d) ? "black" : "";
        }).attr("stroke-width", function (d) {
          return checkHighlight(d) ? 2 : "";
        }).transition().duration(1000).delay(1000).attr("opacity", function (d) {
          return firstLevelHighlights.includes(d.id) ? 1.0 : 0.4;
        }).attr("width", function (d) {
          return d.weight ? squareSide + d.weight * squareSide / maxWeight : squareSide;
        }).attr("height", function (d) {
          return d.weight ? squareSide + d.weight * squareSide / maxWeight : squareSide;
        });
        highlightSelectedPatterns();
        decidePrintButtonVisibility();
      } else if (ctrl.prev_highlight_text) {
        ctrl.searchedFiles = [];
        decidePrintButtonVisibility();
        circleUpdate.attr("stroke", "").attr("stroke-width", "").attr("opacity", 1.0);

        rectUpdate.attr("stroke", "").attr("stroke-width", "").attr("opacity", 1.0);
      }

      var renderPerTicks = 20;
      function ticked() {
        if (renderPerTicks == 20) {
          var transformXY = function transformXY(d) {
            return "translate(" + d.x + "," + d.y + ")";
          };

          linkUpdate.attr("x1", function (d) {
            return d.source.x;
          }).attr("y1", function (d) {
            return d.source.y;
          }).attr("x2", function (d) {
            return d.target.x;
          }).attr("y2", function (d) {
            return d.target.y;
          });

          //circleUpdate.attr("transform", transformXY);
          circleUpdate.attr("cx", function (d) {
            return d.x;
          }).attr("cy", function (d) {
            return d.y;
          });

          triangleUpdates.attr("transform", transformXY);
          rectUpdate.attr("transform", transformXY);
        }
        renderPerTicks -= 1;
        if (renderPerTicks === 0) {
          renderPerTicks = 20;
        }
      }

      var checkIssues = false;
      var checkFiles = false;

      function decideColorOnClick(hash, d, searchKeys, nodeType) {
        if (hash[d.id]) {
          return "red";
        } else if (searchKeys.some(function (el) {
          return totals[d.id].links.includes(el);
        })) {
          return "green";
        } else {
          if (nodeType === "circle") {
            return checkIssues ? checkHighlightIssues(d) ? "black" : "" : "";
          }
          if (nodeType === "rect") {
            return checkFiles ? checkHighlight(d) ? "black" : "" : "";
          }
          if (nodeType === "triangle") {
            return "";
          }
        }
      }

      function highlightSelectedPatterns() {
        if (ctrl.issues_highlight_text) {
          issues_highlight_text = ctrl.issues_highlight_text.toLowerCase();
          issues_regex = new RegExp(issues_highlight_text);
          checkIssues = true;
        }

        if (ctrl.highlight_text) {
          highlight_text = ctrl.highlight_text.toLowerCase();
          files_regex = new RegExp(highlight_text);
          checkFiles = true;
        }
        var searchKeys = Object.keys(ctrl.highlightedRects).concat(Object.keys(ctrl.highlightedCircles)).concat(Object.keys(ctrl.highlightedTriangles));

        svg.select(".nodes").selectAll("circle").transition().duration(1000).attr("stroke", function (d) {
          return decideColorOnClick(ctrl.highlightedCircles, d, searchKeys, "circle");
        }).attr("stroke-width", function (d) {
          return decideColorOnClick(ctrl.highlightedCircles, d, searchKeys, "circle") !== "" ? 2 : "";
        });

        svg.select(".nodes").selectAll("path").transition().duration(1000).attr("stroke", function (d) {
          return decideColorOnClick(ctrl.highlightedTriangles, d, searchKeys, "triangle");
        }).attr("stroke-width", function (d) {
          return decideColorOnClick(ctrl.highlightedTriangles, d, searchKeys, "triangle") !== "" ? 2 : "";
        });
        svg.select(".nodes").selectAll("rect").transition().duration(1000).attr("stroke", function (d) {
          return decideColorOnClick(ctrl.highlightedRects, d, searchKeys, "rect");
        }).attr("stroke-width", function (d) {
          return decideColorOnClick(ctrl.highlightedRects, d, searchKeys, "rect") ? 2 : "";
        });

        svg.select(".links").selectAll("line").transition().duration(1000).attr("fill", function (d) {
          return searchKeys.some(function (el) {
            return d.source.id == el || d.target.id == el;
          }) ? "green" : "";
        }).attr("stroke", function (d) {
          return searchKeys.some(function (el) {
            return d.source.id == el || d.target.id == el;
          }) ? "green" : grafanaBootData.user.lightTheme ? "black" : "white";
        }).attr("stroke-width", function (d) {
          return searchKeys.some(function (el) {
            return d.tooltip.includes(el);
          }) ? getLinkThickness(d) + 1 : getLinkThickness(d);
        });
      }

      function dragstarted(d) {
        if (!d3.event.active) simulation.alphaTarget(0.3).restart();
        d.fx = d.x;
        d.fy = d.y;
      }

      function dragged(d) {
        d.fx = d3.event.x;
        d.fy = d3.event.y;
      }

      function clicked(d, i) {
        if (!d3.event) return;

        if (d3.event.srcElement.nodeName !== "circle" && d3.event.srcElement.nodeName !== "rect" && d3.event.srcElement.nodeName !== "path") {
          return;
        }

        if (d3.event.srcElement.nodeName === "circle") {
          if (ctrl.highlightedCircles[d.id]) {
            delete ctrl.highlightedCircles[d.id];
          } else {
            ctrl.highlightedCircles[d.id] = true;
          }
        }

        if (d3.event.srcElement.nodeName === "rect") {
          if (ctrl.highlightedRects[d.id]) {
            delete ctrl.highlightedRects[d.id];
          } else {
            ctrl.highlightedRects[d.id] = true;
          }
        }

        if (d3.event.srcElement.nodeName === "path") {
          if (ctrl.highlightedTriangles[d.id]) {
            delete ctrl.highlightedTriangles[d.id];
          } else {
            ctrl.highlightedTriangles[d.id] = true;
          }
        }

        highlightSelectedPatterns();
        if (_.isEmpty(ctrl.highlightedCircles) && _.isEmpty(ctrl.highlightedRects) && _.isEmpty(ctrl.highlightedTriangles)) {
          d3.select("#clearButton").style("display", "none");
        } else {
          d3.select("#clearButton").style("display", "block");
        }
        decidePrintButtonVisibility();
        d3.event.stopPropagation();
      }

      function decidePrintButtonVisibility() {
        if (_.isEmpty(ctrl.highlightedCircles) && _.isEmpty(ctrl.highlightedRects) && _.isEmpty(ctrl.highlightedTriangles) && _.isEmpty(ctrl.searchedFiles) && _.isEmpty(ctrl.searchedRefs)) {
          d3.select("#printHighlightedFiles").style("display", "none");
          d3.select("#showHighlightedFiles").style("display", "none");
        } else {
          d3.select("#printHighlightedFiles").style("display", "block");
          d3.select("#showHighlightedFiles").style("display", "block");
        }
      }

      function dragended(d) {
        if (!d3.event.active) simulation.alphaTarget(0);
        d.fx = null;
        d.fy = null;
      }

      function initHash(hash, key) {
        if (!hash[key]) {
          hash[key] = {};
          return true;
        }

        return false;
      }

      function setTotalsHash(hash, key, value, link) {
        if (initHash(hash, key)) {
          hash[key].count = 0;
          hash[key].value = 0;
          hash[key].links = [];
        }
        hash[key].value += value;
        hash[key].links.push(link);
        hash[key].count++;
      }

      function getGroup(d, index) {
        var group;
        var default_index = index === 0 ? default_index1 : default_index2;
        var selector = index === 0 ? color_data_index1 : color_data_index2;
        var regExp = index === 0 ? color_regexp1 : color_regexp2;

        var selectorData;

        if (selector !== null) selectorData = d[selector];

        if (regExp) {
          var result = regExp.exec(d[index]);

          if (result && result.length) selectorData = result[result.length - 1];
        }

        if (selectorData) {
          group = colorSelections[selectorData];
          if (group === undefined) {
            group = colorSelections[selectorData] = columns.length;
            columns.push({
              text: selectorData,
              group: index
            });
            if (keyValueColorHash[selectorData]) {
              nodeNameIndexHash[group] = selectorData;
            }
          }
        } else group = default_index;

        return group;
      }

      function getAdditionalGroup(d, index) {
        var group;
        var default_index = 0;
        var selector = index === 0 ? additionalDataListIndex : additionalDataListSecondIndex;
        var regExp = index === 0 ? additionalDataListRegexp : additionalDataListSecondRegexp;

        var selectorData;

        if (selector !== null) selectorData = d[selector];

        if (regExp) {
          var result = regExp.exec(d[index]);

          if (result && result.length) selectorData = result[result.length - 1];
        }

        if (selectorData) {
          group = colorSelections[selectorData];
          if (group === undefined) {
            group = colorSelections[selectorData] = columns.length;
            columns.push({
              text: selectorData,
              group: -1
            });
            if (keyValueColorHash[selectorData]) {
              nodeNameIndexHash[group] = selectorData;
            }
          }
        } else group = default_index;

        return group;
      }

      function getTotalTooltip(key, weight) {
        var totalObj = totals[key];
        return "<br/> Total: " + totalObj.value + "&nbsp;&nbsp; Edge Count: " + totalObj.count + ("&nbsp;&nbsp; " + panel.locs_metric_name + ": ") + weight || 0;
      }
    }

    function render() {
      data = ctrl.data;
      panel = ctrl.panel;
      additionalData = ctrl.data2;
      additionalDataList = ctrl.additionalDataList;
      couplingData = ctrl.couplingData;
      isOthersSelected = ctrl.isOthersSelected;
      if (setElementHeight()) if (ctrl._error || !data || !data.length) {
        showError(ctrl._error || "No data points");

        data = [];
        addNetworkChart();
      } else {
        addNetworkChart();
        showError(false);
      }

      ctrl.renderingCompleted();
    }
  }

  _export("default", link);

  return {
    setters: [function (_lodash) {
      _ = _lodash.default;
    }, function (_appCoreApp_events) {
      appEvents = _appCoreApp_events.default;
    }],
    execute: function () {}
  };
});
//# sourceMappingURL=rendering.js.map
